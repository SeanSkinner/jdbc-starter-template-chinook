package no.noroff.sean.postgresjdbcdemo.data;

import no.noroff.sean.postgresjdbcdemo.models.Customer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerRepository {
    @Value("${spring.datasource.url}")
    private String url;

    public List<Customer> getCustomers() {
        String sql = "SELECT first_name FROM customer";
        List<Customer> customers = new ArrayList<>();
        try (Connection con = DriverManager.getConnection(url);
             PreparedStatement ps = con.prepareStatement(sql)) {
            try (ResultSet rs = ps.executeQuery()) {
                while(rs.next()) {
                    customers.add(new Customer(rs.getString("first_name")));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customers;
    }
}
