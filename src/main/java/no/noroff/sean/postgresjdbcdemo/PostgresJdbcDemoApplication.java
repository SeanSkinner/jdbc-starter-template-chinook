package no.noroff.sean.postgresjdbcdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PostgresJdbcDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(PostgresJdbcDemoApplication.class, args);
    }

}
