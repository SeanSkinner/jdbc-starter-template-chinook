package no.noroff.sean.postgresjdbcdemo.models;

public class Customer {
    private String firstName;

    public Customer(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
