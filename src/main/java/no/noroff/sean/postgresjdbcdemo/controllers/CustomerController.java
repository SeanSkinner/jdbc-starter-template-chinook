package no.noroff.sean.postgresjdbcdemo.controllers;

import no.noroff.sean.postgresjdbcdemo.data.CustomerRepository;
import no.noroff.sean.postgresjdbcdemo.models.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerController {
    private CustomerRepository customerRepository;


    @Autowired
    public CustomerController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @GetMapping("/customers")
    public List<Customer> getAllCustomers() {
        return customerRepository.getCustomers();
    }
}
